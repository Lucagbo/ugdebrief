<?php header('Content-type: text/html; charset=utf-8');
// costants: links et cles pour le webServices de UG France
define('OPERATIONS', 'https://www.yopcharlie.com/bpur/ws/ope.json.php');
define('TRAIL', 'https://www.yopcharlie.com/bpur/ws/listing.defi.json.php?');
define('CHALLANGES', 'https://www.yopcharlie.com/bpur/ws/listing.challenges.json.php');
define('IMAGES', 'https://www.yopcharlie.com/bpur/trails/');
define('CLE_UGCH', '8542f2dd2db9841641a57d26ae73837123a3bbb9');

function get_web_page($url)//pour conturner le https il faut utiliser curl
{
	$options = array(CURLOPT_RETURNTRANSFER => true, // return web page
	CURLOPT_HEADER => false, // don't return headers
	CURLOPT_FOLLOWLOCATION => true, // follow redirects
	CURLOPT_ENCODING => "", // handle all encodings
	CURLOPT_USERAGENT => "spider", // who am i
	CURLOPT_AUTOREFERER => true, // set referer on redirect
	CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
	CURLOPT_TIMEOUT => 120, // timeout on response
	CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
	CURLOPT_SSL_VERIFYPEER => false // Disabled SSL Cert checks
	);

	$ch = curl_init($url);
	curl_setopt_array($ch, $options);
	$content = curl_exec($ch);
	$err = curl_errno($ch);
	$errmsg = curl_error($ch);
	$header = curl_getinfo($ch);
	curl_close($ch);

	$header['errno'] = $err;
	$header['errmsg'] = $errmsg;
	$header['content'] = $content;//html_entity_decode(
	return $header;
}

function get_Operations() {

	//Array utilise dans la functions
	$arrayOp = array();
	// il aura a l'interieur les trails aussi
	$jsonOp = get_web_page(OPERATIONS . '?key=' . CLE_UGCH);
	//Get json operations
	$objOp = json_decode($jsonOp['content']);

	//stdobject devien array
	$arrayOpTmp = json_decode(json_encode($objOp), true);
	// crer un array de defis
	foreach ($arrayOpTmp as $key => $operation) {
		$arrayOp[$operation['id']] = $operation;
	
	}
	return $arrayOp;

}

//------------------------------------------------------RequeteTrails--------------------------------------------------------------------------------------

function get_Operation($id, $code) {//$op_id de l'evenment choisi par l'utilisateur
	//Get json operations avec l'id de l'événement choisi
	$jsonOp = get_web_page(OPERATIONS . '?key=' . CLE_UGCH . '&code=' . $code);
	
	$objOp = json_decode($jsonOp['content']);

		
	$arrayOpTmp = json_decode(json_encode($objOp), true);
	
	//creer un array équipes
	$arrayEquipes = array();
	$arrayEquipesTmp = $arrayOpTmp[0]['equipes'];

	foreach ($arrayEquipesTmp as $key => $value) {
		$arrayEquipes[$value['code_equipe']] = $value;
	}
	sort($arrayEquipes);

	//creer l'array categorie avec l'id comme cle
	$arrayCategoriesTmp = $arrayOpTmp[0]['categories'];
	$arrayCategories = array();
	foreach ($arrayCategoriesTmp as $key => $value) {
		$arrayCategories[$value['cat_id']] = $value;
	}
	$arrayTrail = array();
	$arraySlide = array();
	$arrayMedia = array();
	$superArray = array();
	$arrayPoints = array();

	$jsonTrail = get_web_page(TRAIL . '?key=' . CLE_UGCH . '&id=' . $id);
	//$idEvent=$_GET[$idEvent], Get json trails

	$objTrail = json_decode($jsonTrail['content']);
	//decode
	
	//transforme le stdobject en array
	$arrayTrailTemp = json_decode(json_encode($objTrail), true);

	foreach ($arrayTrailTemp as $key => $valueTrail) {//Pour-chaque defi il vais chercher ces slide GetSlideChallenge-------------------------------------------------------------------------------------

		//$arrayOp[$valueTrail['up_id']]['trails'][$valueTrail['trek']] = $valueTrail;

		$jsonChallenge = get_web_page(CHALLANGES . '?key=' . CLE_UGCH . '&id=' . $valueTrail['trek']);
		//messo il treck

		$objChallenge = json_decode($jsonChallenge['content']);
		$arraySlideTemp = json_decode(json_encode($objChallenge), true);

		//met l'id come cles de l array
		$arrayTrail[$valueTrail['trek']] = $valueTrail;

		//vais prende et sauve garde les images.
		sort($arraySlideTemp);

		//---------------------Récupere les image depuis la bacjk office de UGFrance---Attentions prends beacoup du temps-----------------------------------------------------------------------------------
		/*$url = IMAGES . $arraySlideTemp[0]['defi_image'];

		//prends l;es immages pour le premiere slides
		$imageContent = get_web_page($url);
		file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/UGD/img/defis/' . $arraySlideTemp[0]['defi_id'] . '.jpeg', $imageContent['content']);
*/
		foreach ($arraySlideTemp as $keySlide => $valueSlide) {

			//ajoute à l'array trail les defis titre
			$arrayTrail[$valueSlide['trek_id']]['slides'][] = $valueSlide;

		}

	}

	// cree le super array avec tous les donnes pour l'initialisations organiser
	 $superArray = array('idJeu' => $arrayOpTmp[0]['id'], 'nomJeu' => $arrayOpTmp[0]['nom'], 'equipes' => $arrayEquipes, 'categories' => $arrayCategories, 'trails' => $arrayTrail);
     return($superArray);
	/*//sauvegarde le file json
	$jsonArrayUG = json_encode($superArray);
	$fp = fopen($_SERVER['DOCUMENT_ROOT'] . "/UGD/jsonEventUG.txt", "wb");
	fwrite($fp, $jsonArrayUG);
	fclose($fp);

	return $superArray;*/

	/*	foreach ($superArray[slides] as $key => $value) {

	 }
	 $pathFolder ='/DATA/'.$arraySlide[''];

	 if (file_exists($filename)){

	 }*/

	 
	//$rocky=$superArray['trails']['5598']['slides'][0]['defi_titre'];
	
	

	
	}
function stripAccents($str) {
 return strtr(utf8_decode($str), utf8_decode('àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ'), 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
 }

 function charge_files_media() {
 $media = array();
 $arrayPoints=array();
 $directories = array();
 $root=('DATA');
 
 $last_letter = $root[strlen($root) - 1];
 $root = ($last_letter == '\\' || $last_letter == '/') ? $root : $root . DIRECTORY_SEPARATOR;

 $directories[] = $root;

 while (sizeof($directories)) {

 $dir = array_pop($directories);

 if ($handle = opendir($dir)) {
 while (false !== ($file = readdir($handle))) {
 if ($file == '.' || $file == '..' || $file == '.DS_Store' || $file == '.DS' || strstr($file, '###')) {// prende i file

 continue;
 }
/*
$newDir= stripAccents($dir);
$newFile=stripAccents($file);

 $file = $dir . $file;// 
print_r($file);
 $newFile = stripAccents($file);
 rename(utf8_decode($file), utf8_decode($newFile));
 $file = $newFile;
*/
 //$dir = iconv("iso-8859-1", "utf-8", $dir);

				//$file = iconv("iso-8859-1", "utf-8", $file);
 $file = $dir . $file;


//$file = iconv('UTF-8', 'US-ASCII//TRANSLIT', $file);
 // se no prende riscrive una dopo l'altra

 if (is_dir($file)) {//  controlla se esiste il file
 $directory_path = $file.DIRECTORY_SEPARATOR;
 array_push($directories, $directory_path);
 } elseif (is_file($file)) {
 $dire=str_replace('DATA'.DIRECTORY_SEPARATOR,'' , $dir);
 $dire = iconv("iso-8859-1", "utf-8", $dire);
 //$newDir= stripAccents($dire);
 //creé l'array files
 $file = iconv("iso-8859-1", "utf-8", $file);

 $files['files'][str_replace(DIRECTORY_SEPARATOR,'',$dire)][] = $file;


 }
 
 }
		
 closedir($handle);
 }
 }
/* ------------------------------------------------------------------- Mette nel super array l'array files------------------------------------------------------------------ 
foreach($array['trails'] as $keyTrails => $valueTrails){
	
foreach ($files as $keyfile => $valuefile) {
	
}
foreach ($valueTrails['slides'] as $keySlide => $valueSlide) {
		
	if ($valueSlide['defi_type'] == '8' || $valueSlide['defi_type'] == '2') {
		//Cle va mettre dans l'array les donées de $files récuperer depuis les tablettes
		$array['trails'][$valueSlide['trek_id']]['media'][$valueSlide['defi_titre']] = $files['files'][$valueSlide['defi_titre']];
	//	if($valueSlide['defi_titre']==$files['files'][$keyfile]){
			
			//	$array['trails'][$valueSlide['trek_id']]['media'][$valueSlide['defi_titre']] = $files['files'][$keySlide];
			}	//}
//$valueMedia[]=$file[$keyMedia];

}
}*/

return($files);  
}
//return($array);
//


//------------------------------------------ARRAY PER I PUNTI-------------------------------------------------------------------------------
function charge_points(){//$array

	$filename = "DATA\points.txt";
	$handle = fopen($filename, "r");
	$contents = fread($handle, filesize($filename));
	fclose($handle);

	$arrayPointsJeuTemp = json_decode($contents);

	$arrayPointsJeu = json_decode(json_encode($arrayPointsJeuTemp), true);
	$pointot = array();
	foreach ($arrayPointsJeu as $keyPointT => $valuePointT) {

		$pointot[$keyPointT] = array_sum($valuePointT);

		/*
		 foreach ($valuePointT as $keyPointE => $valuePointE) {

		 print_r($valuePointE);

		 }*/
	}


	return ($arrayPointsJeu);

}
if (isset($_POST['callchargepoint'])) {
   $point = charge_points();
	$pointJS= json_encode($pointr);
	print $pointJS;
}

/* ------------------------------------------------------------------- Mette nel super array l'array files------------------------------------------------------------------

 //$dir = iconv("iso-8859-1", "utf-8", $dir);
				//$file = iconv("iso-8859-1", "utf-8", $file);

				
	/* Genera il ficher points;
	 $arrayEquipes = $array['equipes'];
	 $arrayDefi = $array['trails'];
	 $arrayPointsJeu = array();
	 $arrayPointsDefi = array();
	 $point =0 ;
	 foreach($arrayEquipes as $keyEquipe => $valueEquipe){

	 foreach($arrayDefi as $keyDefi => $valueDefi){
	 $arrayPointsJeu= $arrayPointsDefi;
	 $arrayPointsDefi[ $valueEquipe['code_equipe']][$valueDefi['nom']]= $point;

	 }
	 }
	 $jsonPoint=json_encode($arrayPointsJeu);
	 print_r($jsonPoint);*/