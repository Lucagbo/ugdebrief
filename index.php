<!DOCTYPE html>
<html lang="en">
	<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<?php header('Content-Type: text/html; charset=utf-8');
		//prende la pagina php delle funzioni
		//mette l'array con i files nella variabile Sdata
 ?>


		<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
		Remove this if you use the .htaccess -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title>index</title>
		<meta name="description" content="">
		<meta name="Luciano" content="UGD">

		<meta name="viewport" content="width=device-width; initial-scale=1.0">

		<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
		
		<link rel="shortcut icon" href="/favicon.ico">
		<link rel="apple-touch-icon" href="/apple-touch-icon.png">
		<link rel="stylesheet" type="text/css" href="css/style.css"/>
		<link rel="stylesheet" type="text/css" href="css/lightview/lightview.css"/>
		
		</head>
		

	<body>



			<section> 
			<header>
			
					<?php include("fonctions.php")?>


		<?php if(isset($_GET['op_id'])): 
				$array = get_Operation($_GET['op_id'], $_GET['code']);
				include ("menu.php");
				//$_POST['page']=='defis';
				$arrayDefi = $array['trails'];
				$arrayCategorie = $array['categories'];
				$arrayEquipes = $array['equipes']; 
				
				//$arrayMedia = $array['media'];;?>
			
		<?php $arrayMedia = charge_files_media(); ?>
	

		<?php $arrayPoints = charge_points();
	
?>
		
				
				<h1>Debriefing Urban Gaming - <?php print $array['nomJeu']?></h1>

				
			</header>
			
	<?php  if(isset($_GET['page']) && $_GET['page'] == 'classement'): ?>
	
 <button id="displayC">Display</button>

	<table id="classement">
	<thead>
	<tr>
	<th>Positions</th>
	<th>Nom d'équipe</th>
	<th>Points</th>
	<th>&nbsp;</th>
	</tr>
	</thead>
	<tbody>
	<?php $pointot = array(); ?>
	<?php $i=1;foreach($arrayPoints as $keyPointT => $valuePointT):
	?>
	<?php $pointot[$keyPointT] = array_sum($valuePointT); ?>
	<tr data-point="" data-equipe="<?php print($keyPointT)?>">
	<td class="position"><?php print $i; ?>
	</td>
	<td><?php print $keyPointT
	?>
	</td>
	<td class="point">
	<?php print $pointot[$keyPointT]
	?>
	</td>
	<td>
	<div class="btn-group">
	<button type="button" class="btn btn-default btn-xs" data-equipe="<?php print $keyPointT?>" data-op="reset"><span></span></button>
	<button type="button" class="btn btn-default btn-xs" data-equipe="<?php print $keyPointT?>" data-op="minus"><span class="icon-minus"></span></button>
	<button type="button" class="btn btn-default btn-xs" data-equipe="<?php print $keyPointT?>" data-op="plus"><span class="icon-plus"></span></button>
	</div>
	</td>
	</tr>

	<?php $i++;
	endforeach;
	?>
	</tbody>
	</table>

	<?php endif ?>

	<?php if(isset($_GET['page']) && $_GET['page'] == 'defis'):
	?>
	<div id="defis">

	<button id="test" class="lightview"  href="img/video_camera.jpg">TEST</button>

	<?php foreach ($arrayDefi as $keyDefi => $valueDefi):
	?>
	<div class="defi" >
	<div class="visible">
	<!--------------------------------------Affiche Image trail------------------------------------>

	<img class="imgTrail" src= <?php print 'img/defis/'.$valueDefi['slides'][0]['defi_id'].'.jpeg'?> />

	<!------------------------------------Affiche Nom trail-------------------------------------------------------------------->
	<h3><?php print $valueDefi['nom']; ?>
	</h3>
	<!------------------------------------Affiche les equipes------------------------------------------------------------------------>

	

	<table id="Parteciper">
	<tbody>

	<tr>
	<td>Nr Equipe</td>			
	<?php foreach ($arrayEquipes as $keyEq => $valueEq):?>
	<td class="pointPardefis" data-points="<?php print $arrayPoints[$valueEq['code_equipe']][$valueDefi['nom']]; ?>">
		<?php print $valueEq['numero_equipe']; ?>"
		</td>

	<?php endforeach ?>
	</tr>
	
	<!--tr class="PointPasVis">
	<td>
	Points	
	</td>
		<?php foreach ($arrayEquipes as $keyEq => $valueEq):?>
	<td class="pointPardefis">
	</td>	
		<?php endforeach; ?>
	</tr-->

	</tbody>
	</table>


	<!------------------------------------Affiche categorie------------------------------------------------------------------>

	<img src="https://www.yopcharlie.com/bpur/<?php echo $arrayCategorie[$valueDefi['categorie']]['cat_icone']?>"  class="cat" />

	<?php foreach ($valueDefi['slides'] as $keySlides => $valueSlides):
	?>

	<?php if($valueSlides['defi_type']=='8'):
	?>
	<button class="showHide">showHide</button>

	<img class="icon" src=img/video_camera.jpg />

	<?php elseif($valueSlides['defi_type']=='2'): ?>
	<button class="showHide">showHide</button>

	<img class="icon"src=img/photo_camera.jpg />

	<?php endif ?>

	<?php endforeach ?>

	</div>
	<div class="pasvis">

	<!-----------------------------------------Affiche label pour les defis video et photo---------------------------------------------------------------->
	<?php foreach ($valueDefi['slides'] as $keySlides => $valueSlides):
	?>

	<!---------------------------------------Affiche label & icon pour les defis de type video/photo---------------------------------------------------------------------->

	<?php if($valueSlides['defi_type']=='8'):
	?>
	<p class="desc"><?php print $valueSlides['defi_desc']?>'
	</p>

	<img class="icon" src=img/video_camera.jpg />
	<?php //echo $valueSlides['defi_titre']; print_r($arrayMedia['files']); die(); ?>

	<?php //$nome = stripAccents($valueSlides['defi_titre']); ?>
	<?php //print_r(mb_detect_encoding($nom)); exit; ?>
	<?php foreach ($arrayMedia['files'][$valueSlides['defi_titre']] as $keyM => $valueM):

	?>

	<div>

	<a class="media lightview" href="<?php print $valueM?>">
	<video controls>
	<source src="<?php print $valueM?>">
	</video>
	</a>
	<p></p>
	<button type="button" class="btn btn-default btn-xs" data-equipe="<?php print $keyPointT?>" data-op="minus"><span class="icon-minus"></span></button>
	<button type="button" class="btn btn-default btn-xs" data-equipe="<?php print $keyPointT?>" data-op="plus"><span class="icon-plus"></span></button>
	</div>

	<?php endforeach; ?>

	<?php elseif($valueSlides['defi_type']=='2'): ?>
	<p class="desc"><?php print $valueSlides['defi_desc']?><
	/p>
	<?php $nom = iconv("iso-8859-1", "utf-8", $valueSlides['defi_titre']); ?>
	<?php foreach ($arrayMedia['files'][$nom] as $keyM => $valueM): //$valueSlides['defi_titre']
	?>
	<img class="media lightview" href="<?php print $valueM?>" src="<?php print $valueM?>"/>

	<?php endforeach; ?>

	<?php endif ?>

	<?php endforeach ?>
	</div></div>
	<?php endforeach; ?>

	<?php endif ?>

	</div>

	<!--------------------------------------------------MenuInitial choix de l'événment---------------------------------------->

	<?php else: $arrayOP = get_Operations(); ?>
	<h2> Choissisez l événment UrbanGaming à charger?</h2>

	<?php foreach($arrayOP as $key => $operation): //affiche les nome des événements
	?>
	<div class="nomsEvents" >
	<a href="?page=defis&op_id=<?php print $operation['id']; ?>&code=<?php print $operation['code']; ?>">
	<?php print $operation['nom']; ?>
	</a></div>
	<?php endforeach; ?>
	<?php endif; ?>

	</div>

	</section>

	<footer>
	<p >
	&copy; Travail Bachelor 2014
	</p>
	</footer>

	<script src="js/jquery.js" type="text/javascript"></script>
	<script src="js/jquery.sortElements.js" type="text/javascript" ></script>
	<script type="text/javascript" src="js/swfobject.js"></script>
	<script type="text/javascript" src="js/spinners.min.js"></script>
	<script type="text/javascript" src="js/lightview.js"></script>
	<script src="js/fonction.js" type="text/javascript"></script>

	</body>
</html>
